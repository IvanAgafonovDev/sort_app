import copy
import re
from django.contrib import messages
from django.shortcuts import redirect, render
from django.views.generic import View
from .forms import UserFilesForm
from .models import SortResult
from .sort_lib import BubbleSort, InsertionSort, MergeSort, SelectionSort  # we use these classes dynamically


class IndexPage(View):
    def get(self, request):
        return render(request, 'sort_app/index_page.html', context={'form': UserFilesForm})

    def post(self, request):
        if request.FILES.get('unsorted_file').name[-3:] != 'txt':
            messages.info(request, 'Only .txt files are supported!')
            return redirect('index_page_url')

        form = UserFilesForm(request.POST, request.FILES)
        if form.is_valid():
            user_files = form.save()
            with open(user_files.unsorted_file.path) as unsorted_file:
                unsorted_input = [int(str_) for str_ in re.findall(r'-?\d+', unsorted_file.read())]  # only numbers

            sorted_input, time_taken = globals()[request.POST.get('algorithm')].sort(copy.deepcopy(unsorted_input))
            # in the global namespace we look for the proper class to perform a chosen sort

            last_sort = SortResult.objects.create(algorithm=request.POST.get('algorithm'),
                                                  input=unsorted_input,
                                                  output=sorted_input,
                                                  time=time_taken)

            return render(request, 'sort_app/index_page.html', context={'form': UserFilesForm,
                                                                        'last_sort': last_sort})
        return redirect('index_page_url')
