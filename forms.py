from django.forms import ModelForm
from .models import UserFiles


class UserFilesForm(ModelForm):
    class Meta:
        model = UserFiles
        fields = ['unsorted_file']

    def __init__(self, *args, **kwargs):
        super(UserFilesForm, self).__init__(*args, **kwargs)
        self.fields['unsorted_file'].widget.attrs.update({'style': 'display: block;'})

