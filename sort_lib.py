from abc import ABC, abstractmethod
from functools import wraps
import time


def d_timer(func):
    @wraps(func)
    def wrapper(*args):
        start = time.time()
        result = func(*args)
        return result, ("%.5f" % abs(start - time.time()) + ' sec')  # time is str like '0.00004 sec'
    return wrapper


class Sort(ABC):
    @abstractmethod
    def sort(cls, uns_list):
        pass

    @classmethod
    def hello_world(cls):
        print('We use {} class for sorting lists.'.format(cls.__name__))


class SelectionSort(Sort):
    @classmethod
    @d_timer
    def sort(cls, uns_list):
        next_iter = False if len(uns_list) <= 1 else True
        sort_start = 0

        while next_iter:
            min_value = float('inf')  # just a funky initial stuff
            position = len(uns_list) - 1

            for i in range(sort_start, len(uns_list)):
                if uns_list[i] < min_value:
                    min_value = uns_list[i]
                    position = i

            uns_list[sort_start], uns_list[position] = uns_list[position], uns_list[sort_start]
            sort_start += 1

            if sort_start == len(uns_list) - 1:
                next_iter = False
        return uns_list


class BubbleSort(Sort):
    @classmethod
    @d_timer
    def sort(cls, uns_list):

        next_iter = False if len(uns_list) <= 1 else True

        while next_iter:
            swapped = False

            for i in range(len(uns_list) - 1):
                if uns_list[i] > uns_list[i + 1]:
                    uns_list[i], uns_list[i + 1] = uns_list[i + 1], uns_list[i]
                    swapped = True

            if swapped is False:
                next_iter = False
        return uns_list


class InsertionSort(Sort):
    @classmethod
    @d_timer
    def sort(cls, uns_list):

        will_sort = False if len(uns_list) <= 1 else True

        if will_sort:
            sort_end = 1

            for _ in range(len(uns_list)):
                if uns_list[sort_end] > uns_list[sort_end - 1]:
                    sort_end += 1
                else:
                    sort_elem = sort_end

                    for _ in range(sort_end):
                        if uns_list[sort_elem - 1] > uns_list[sort_elem]:
                            uns_list[sort_elem - 1], uns_list[sort_elem] = uns_list[sort_elem], uns_list[sort_elem - 1]
                            sort_elem -= 1
                        else:
                            sort_end += 1
                            break
        return uns_list


class MergeSort(Sort):
    @classmethod
    @d_timer
    def sort(cls, uns_list):

        def divider(uns_list):
            if len(uns_list) <= 1:
                return uns_list

            mid_point = len(uns_list) // 2
            left = divider(uns_list[:mid_point])
            right = divider(uns_list[mid_point:])
            return merger(left, right)

        def merger(left, right):
            sorted_list = []

            while len(left) > 0 and len(right) > 0:
                if left[0] <= right[0]:
                    sorted_list.append(left.pop(0))
                else:
                    sorted_list.append(right.pop(0))

            if len(left) > 0:
                sorted_list += left
            if len(right) > 0:
                sorted_list += right
            return sorted_list
        return divider(uns_list)

