from django.db import models


class SortResult(models.Model):
    algorithm = models.CharField(max_length=25, null=True)
    input = models.TextField(null=True)
    output = models.TextField(null=True)
    time = models.CharField(max_length=25, null=True)

    def __str__(self):
        return '{}. {}'.format(self.id, self.algorithm)


class UserFiles(models.Model):
    unsorted_file = models.FileField(upload_to='sort_app/files/', null=True)






