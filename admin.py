from django.contrib import admin
from .models import SortResult


class SortResultAdmin(admin.ModelAdmin):
    list_display = ('algorithm', 'input', 'output', 'time')
    list_filter = ('algorithm', )
    search_fields = ('algorithm', 'input', 'output', 'time')
    sortable_by = ('algorithm', 'input', 'output', 'time')


admin.site.register(SortResult, SortResultAdmin)


