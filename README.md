1) Start a new virtual environment and install Django there.  
2) Create a new directory in your Django project root folder and name it "sort_app".  
3) Copy all these repo files inside the folder.  
4) Go to settings.py and add 'sort_app' to your INSTALLED_APPS.  
5) Don't forget to import IndexPage to the main project's urls.py and add this: path('', IndexPage.as_view(), name='index_page_url').  
6) Make migrations, migrate, runserver and check it out!  
